import { h, Component } from 'preact';
import { Router } from 'preact-router';
import Home from '../roots/home/home';
import Error from '../roots/error/error';

export default class App extends Component {
	render() {
		return (
			<div class="app">
				<Router>
                    <Home path="/" />
                    <Error default />
                </Router>
			</div>
		);
	}
}
