import { h, Component } from 'preact';

interface Props { }

interface State { }

class Home extends Component<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  render() {
    const { } = this.props;
    const { } = this.state;

    return (
      <div>Home Works</div>
    )
  }
}

export default Home;
