import './style';
import { h, Component } from 'preact';
import App from './components/app';

export default () => (
    <div>
            <App />
    </div>

);